package com.example.moviestream.core.data

import com.example.moviestream.core.domain.model.MovieGenreItem

val listGenre = listOf(
    MovieGenreItem(
        name = "Horror",
        id = 1
    ),
    MovieGenreItem(
        name = "Comedy",
        id = 2
    ),
    MovieGenreItem(
        name = "Action",
        id = 3
    ),
    MovieGenreItem(
        name = "Drama",
        id = 4
    ),
    MovieGenreItem(
        name = "Thriller",
        id = 5
    )
)